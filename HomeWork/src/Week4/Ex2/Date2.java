/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex2;

/**
 * mm/dd/yyyy
 * @author MathSky
 */
public class Date2{
    private int month;
    private int day;
    private int year;
    
    private static final int[] daysPerMonth={0,31,28,31,30,31,30,31,31,30,31,30,31};
    private static final int[] daysAfterMonth={0,31,59,90,120,151,181,212,243,273,304,334,365};
    private static final int[] daysAfterMonth2={0,31,60,91,121,152,182,213,244,274,305,335,366};
    private static final String[] stringMonth={"","January","February","March","April","May","June","July","August","September","October","November","December"};
    
    public Date2(int month, int day, int year) {
        this.year = year;
        this.month = checkMonth(month);
        this.day = checkDay(day);
        
    }
    public Date2(String sm,int d,int y){
        int tm=0;
        for(int j=1;j<13;j++) if (stringMonth[j].equals(sm)) {tm=j;break;}

            year=year;;month=checkMonth(tm);day=checkDay(d);      
        
    }
    public Date2(int ddd,int y){
        year=y;
        if(isLeapYear(y) && ddd<=366){
            for(int i=1;i<13;i++) if(daysAfterMonth2[i]<=ddd){
                if(daysAfterMonth2[i]==ddd) {month=i;day=daysPerMonth[i];}
                else {month=i+1;day=ddd-daysAfterMonth2[i];}
                break;
            }
        }
        else if(!isLeapYear(y) && ddd<=365){
            for(int i=1;i<13;i++) if(daysAfterMonth[i]<=ddd){
                if(daysAfterMonth[i]==ddd) {month=i;day=daysPerMonth[i];}
                else {month=i+1;day=ddd-daysAfterMonth[i];}
                break;
            }
        }
        else throw new IllegalArgumentException("Sai ngay");
    }
    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }
    private boolean isLeapYear(int y){
        if(y%400==0 || (y%100!=0 && y%4==0)) return true;
        return false;
    }
    private int checkMonth(int m){
        if(m>0 && m<13) return m;
        else throw new IllegalArgumentException("Sai Thang!");
    }
    private int checkDay(int d){
        if(d>0 && d<daysPerMonth[month]) return d;
        if(d==29 && month==2 && (year%400==0 ||(year%4==0 && year%100!=0) )) return d;
        throw new IllegalArgumentException("Sai ngay!");
    }

    public String toString(){
        return String.format("%d/%d/%d", month,day,year);
    }
}