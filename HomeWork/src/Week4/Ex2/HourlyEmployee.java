/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex2;

/**
 *
 * @author MathSky
 */
public class HourlyEmployee extends Employee{
    private double wage;
    private double hours; 

    public HourlyEmployee(double wage, double hours, String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        super(firstName, lastName, socialSecurityNumber, birthday);
        this.wage = wage;
        this.hours = hours;
    }

    public void setWage(double wage) {
        if(wage>=0.0)this.wage = wage;
        else throw new IllegalArgumentException("Hourly wage must be>= 0.0");
    }

    public void setHours(double hours) {
        if ( ( hours >= 0.0 ) && ( hours <= 168.0 ) ) this.hours = hours;
            else
                throw new IllegalArgumentException("Hours worked must be >= 0.0 and <= 168.0" );
    }

    public double getWage() {
        return wage;
    }

    public double getHours() {
        return hours;
    }
    
    // calculate earnings; override abstract method earnings in Employee
    @Override
    public double earnings()
    {
    if ( getHours() <= 40 ) // no overtime
    return getWage() * getHours();
    else
    return 40 * getWage() + ( getHours() - 40 ) * getWage() * 1.5;
    } // end method earnings
    
    // return String representation of HourlyEmployee object
    @Override
    public String toString()
    {
    return String.format( "hourly employee: %s\n%s: $%,.2f; %s: %,.2f",
    super.toString(), "hourly wage", getWage(),
    "hours worked", getHours() );
    } // end method toString
}