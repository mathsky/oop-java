/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex2;

/**
 *
 * @author MathSky
 */
public abstract class Employee implements Week4.Ex6.Payable{
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private Date2 birthday;

    public Employee(String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.birthday = birthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public Date2 getBirthday() {
        return birthday;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public void setBirthday(Date2 birthday) {
        this.birthday = birthday;
    }
    
    @Override
    public String toString(){
        return String.format( "%s %s\nsocial security number: %s\nbirthday: %s",
            getFirstName(), getLastName(), getSocialSecurityNumber(),this.getBirthday().toString() );
    } 
    
    public abstract double earnings();
    
    @Override
    public double getPaymentAmount(){ 
        return earnings();
    }
}