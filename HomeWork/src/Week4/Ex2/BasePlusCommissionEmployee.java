/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex2;

/**
 *
 * @author MathSky
 */
public class BasePlusCommissionEmployee extends CommissionEmployee{
    private double baseSalary; 

    public BasePlusCommissionEmployee(double baseSalary, double grossSales, double commissionRate, String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        super(grossSales, commissionRate, firstName, lastName, socialSecurityNumber, birthday);
        this.baseSalary = baseSalary;
    }
    
    // set base salary
    public void setBaseSalary( double salary )
     {
        if ( salary >= 0.0 )baseSalary = salary;
        else
        throw new IllegalArgumentException( "Base salary must be >= 0.0" );
    }

    public double getBaseSalary() {
        return baseSalary;
    }
    
    // calculate earnings; override method earnings in CommissionEmployee
    @Override
    public double earnings()
    {
        return getBaseSalary() + super.earnings();
    } 
    
    @Override
    public String toString()
    {
        return String.format( "%s %s; %s: $%,.2f",
        "base-salaried", super.toString(),
        "base salary", getBaseSalary() );
    }
}