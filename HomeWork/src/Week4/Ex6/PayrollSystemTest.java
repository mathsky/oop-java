/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex6;

import Week4.Ex2.*;

/**
 *
 * @author MathSky
 */
public class PayrollSystemTest {
    public static void main(String[] args) {
        Date2 d1=new Date2(12,2,1990);
        Date2 d2=new Date2(1,2,1985);
        Date2 d3=new Date2(1,1,1967);
        Date2 d4=new Date2(12,2,1977);
        SalariedEmployee salariedEmployee =new SalariedEmployee( 800.00,"John", "Smith", "111-11-1111",d1 );
        HourlyEmployee hourlyEmployee =new HourlyEmployee(16.75,40, "Karen", "Price", "222-22-2222", d2 );
        CommissionEmployee commissionEmployee =new CommissionEmployee(10000, .06,"Sue", "Jones", "333-33-3333",d3 );
        BasePlusCommissionEmployee basePlusCommissionEmployee =new BasePlusCommissionEmployee( 5000, .04, 300,"Bob", "Lewis", "444-44-4444",d4 );
        
        Employee[] employees = new Employee[ 4 ];
        // initialize array with Employees
        employees[ 0 ] = salariedEmployee;
        employees[ 1 ] = hourlyEmployee;
        employees[ 2 ] = commissionEmployee;
        employees[ 3 ] = basePlusCommissionEmployee;
        
        
        int nowMonth=1;       
        for (Employee cEmployee : employees) {
                System.out.println(cEmployee);              
                if(cEmployee.getBirthday().getMonth()==nowMonth)System.out.printf("earned $%,.2f (happy birthday!)\n\n",cEmployee.earnings()+100 );
                else System.out.printf("earned $%,.2f\n\n",cEmployee.getPaymentAmount());             
            }
    }
}
