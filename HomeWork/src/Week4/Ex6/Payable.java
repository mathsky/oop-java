/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex6;

/**
 *
 * @author MathSky
 */
public interface Payable {
    double getPaymentAmount();
}
