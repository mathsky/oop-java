/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public class Cube extends ThreeDimensionalShape{
    private double a;

    public Cube(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }
    
    public double area(){
        return(4*a*a);
    }
    
    public double volume(){
        return a*a*a;
    }
    
    public String toString(){
        return String.format("Area: %.2f\nVolume: %.2f", this.area(),this.volume());
    }
}
