/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public class ShapeTest {
    public static void main(String[] args) {
        Shape[] shapes= new Shape[2];
        
        shapes[0]= new Square(2);
        shapes[1]= new Cube(2);
        
        for(Shape s:shapes){
            System.out.printf("Shape: %s\n%s\n",s.getClass().getName(),s.toString());
        }
    }
}
