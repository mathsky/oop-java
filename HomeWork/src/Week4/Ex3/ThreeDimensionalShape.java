/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public abstract class ThreeDimensionalShape extends Shape{

    public ThreeDimensionalShape() {
    }
    
    public abstract double area();
    public abstract double volume();
}
