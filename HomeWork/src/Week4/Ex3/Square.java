/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public class Square extends TwoDimensionalShape{
    private double side;

    public Square() {
    }

    
    public Square(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    public double area(){
        return side*side;
    }
    
    public String toString(){
        return String.format("Area: %.2f\n", this.area());
    }
}
