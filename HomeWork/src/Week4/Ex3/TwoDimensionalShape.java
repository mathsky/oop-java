/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public abstract class TwoDimensionalShape extends Shape{

    public TwoDimensionalShape() {
    }
    
    public abstract double area();
}
