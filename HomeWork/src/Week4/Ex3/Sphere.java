/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex3;

/**
 *
 * @author MathSky
 */
public class Sphere extends ThreeDimensionalShape{
    private double radius;

    public Sphere(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    public double area(){
        return(Math.PI*4*radius*radius);
    }
    
    public double volume(){
        return(Math.PI*4*radius*radius*radius/3);
    }
    
    public String toString(){
        return String.format("Area: %.2f\nVolume: %.2f", this.area(),this.volume());
    }
}
