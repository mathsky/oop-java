/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Square extends Quadrilateral{

    public Square() {
    }

    public Square(Point A, Point B, Point C, Point D) {
        super(A, B, C, D);
    }
    
    public Square(Rectangle r){
        super(r.getA(),r.getB(),r.getC(),r.getD());
    }
    
    public double area(){
        return Math.pow(getA().distance(getB()), 2);
    }
}