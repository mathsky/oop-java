/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Quadrilateral {
    private Point A;
    private Point B;
    private Point C;
    private Point D;

    public Quadrilateral() {
    }

    public Quadrilateral(Point A, Point B, Point C, Point D) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
    }

    public boolean isTrapzoid(){
        Vector AB,BC,CD,DA;
        AB= new Vector(getA(),getB());
        BC= new Vector(getB(),getC());
        CD= new Vector(getC(),getD());
        DA= new Vector(getD(),getA());
        if(AB.isParallelTo(CD)||(BC.isParallelTo(DA))) return true;
        return false;
    }

    public Point getA() {
        return A;
    }

    public Point getB() {
        return B;
    }

    public Point getC() {
        return C;
    }

    public Point getD() {
        return D;
    }
}
