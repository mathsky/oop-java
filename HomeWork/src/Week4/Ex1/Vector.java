/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Vector {
    private double x;
    private double y;

    public Vector() {
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Vector(Point p1,Point p2){
        this.x=p2.getX()-p1.getX();
        this.y=p2.getY()-p1.getY();
    }
    
    public boolean isParallelTo(Vector v){
        return(v.x*y==v.y*x);
    }
    
    public boolean isSquareTo(Vector v){
        return (v.x*x+v.y*y==0);
    }
    
    public double length(){
        return Math.sqrt(x*x+y*y);
    }
}
