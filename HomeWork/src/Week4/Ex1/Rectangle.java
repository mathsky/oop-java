/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Rectangle extends Paralellogram{

    public Rectangle() {
    }

    public Rectangle(Point A, Point B, Point C, Point D) {
        super(A, B, C, D);
    }
    
    public Rectangle(Paralellogram p){
        super(p.getA(),p.getB(),p.getC(),p.getD());
    }
    
    public boolean isSquare(){
        Vector AB,BC;
        AB= new Vector(getA(),getB());
        BC= new Vector(getB(),getC());
        return (AB.length()==BC.length());
    }
    
    public double area(){
        return getA().distance(getB())*getA().distance(getD());
    }
}

