/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Trapezoid extends Quadrilateral{
    public Trapezoid() {
    }

    public Trapezoid(Point A, Point B, Point C, Point D) {
        super(A, B, C, D);
        
    }
    
    public Trapezoid(Quadrilateral q){
        super(q.getA(),q.getB(),q.getC(),q.getD());
    }
    
    public boolean isParalellogram(){
        Vector AB,BC,CD,DA;
        AB= new Vector(getA(),getB());
        BC= new Vector(getB(),getC());
        CD= new Vector(getC(),getD());
        DA= new Vector(getD(),getA());
        if(AB.isParallelTo(CD)&&(BC.isParallelTo(DA))) return true;
        return false;
    }
    
    public double area(){ 
        Vector AB,BC,CD,DA;
        AB= new Vector(getA(),getB());
        BC= new Vector(getB(),getC());
        CD= new Vector(getC(),getD());
        DA= new Vector(getD(),getA());
        if(AB.isParallelTo(CD)) {
            double t= (this.getA().getX()-this.getC().getX())*(getC().getY()-getD().getY())-(getA().getY()-getC().getY())*(getC().getX()-getD().getX());
            double h=Math.abs(t)/(getC().distance(getD()));
            return (getA().distance(getB())+getC().distance(getD()))*h/2;
        }
        // BC//DA
        double t= (this.getA().getX()-this.getC().getX())*(getC().getY()-getB().getY())-(getA().getY()-getC().getY())*(getC().getX()-getB().getX());
        double h=Math.abs(t)/(getC().distance(getB()));
        return (getA().distance(getD())+getC().distance(getB()))*h/2;
    }
}
