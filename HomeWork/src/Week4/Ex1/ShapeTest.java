/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class ShapeTest {
    public static void main(String[] args) {
        Quadrilateral q ;
        Point p1,p2,p3,p4;
        p1 =new Point(0,0);
        p2= new Point(2,0);
        p3= new Point(2,1);
        p4= new Point(0,1);      
        q= new Quadrilateral(p1,p2,p3,p4);
        
        if(q.isTrapzoid()) {
            Trapezoid t= new Trapezoid(q);
            System.out.printf("La hinh thang, s=%f\n",t.area());
            
            if(t.isParalellogram()){
                Paralellogram p= new Paralellogram(t);
                System.out.printf("La hinh binh hanh, s=%f\n",p.area());
                
                if(p.isRectangle()){
                    Rectangle r= new Rectangle(p);
                    System.out.printf("La hinh chu nhat, s=%f\n",r.area());
                    
                    if(r.isSquare()) {
                        Square s=new Square(r);
                        System.out.printf("La hinh vuong, s=%f\n",s.area());
                    }
                }
            }
        }
        
        

    }
}
