/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex1;

/**
 *
 * @author MathSky
 */
public class Paralellogram extends Trapezoid{

    public Paralellogram() {
    }

    public Paralellogram(Point A, Point B, Point C, Point D) {
        super(A, B, C, D);
    }
    
    public Paralellogram(Trapezoid t){
        super(t.getA(),t.getB(),t.getC(),t.getD());
    }
    
    public boolean isRectangle(){
        Vector AB,BC;
        AB= new Vector(getA(),getB());
        BC= new Vector(getB(),getC());
        return (AB.isSquareTo(BC));
    }
    
    public double area(){
        double t= (this.getA().getX()-this.getC().getX())*(getC().getY()-getD().getY())-(getA().getY()-getC().getY())*(getC().getX()-getD().getX());
        double h=Math.abs(t)/(getC().distance(getD()));
        return getA().distance(getB())*h;
    }
}
