/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex5;

import Week4.Ex2.Date2;



/**
 *
 * @author MathSky
 */
public class CommissionEmployee extends Employee{
    private double grossSales; // gross weekly sales
    private double commissionRate;

    public CommissionEmployee(double grossSales, double commissionRate, String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        super(firstName, lastName, socialSecurityNumber, birthday);
        this.grossSales = grossSales;
        this.commissionRate = commissionRate;
    }
    
    public void setCommissionRate( double rate )
    {
        if ( rate > 0.0 && rate < 1.0 )commissionRate = rate;
        else
            throw new IllegalArgumentException( "Commission rate must be > 0.0 and < 1.0" );
    } 
    
    // set gross sales amount
    public void setGrossSales( double sales )
        {
            if ( sales >= 0.0 )grossSales = sales;
            else
                throw new IllegalArgumentException( "Gross sales must be >= 0.0" );
       } 

    public double getGrossSales() {
        return grossSales;
    }

    public double getCommissionRate() {
        return commissionRate;
    }
    
    // calculate earnings; override abstract method earnings in Employee
    @Override
    public double getPaymentAmount()
    {
        return getCommissionRate() * getGrossSales();
    } 
  
    @Override
    public String toString()
    {
        return String.format( "%s: %s\n%s: $%,.2f; %s: %.2f","commission employee", super.toString(),
        "gross sales", getGrossSales(),"commission rate", getCommissionRate() );
    } // end method toString
}
