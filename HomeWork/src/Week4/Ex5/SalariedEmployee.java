/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex5;

import Week4.Ex2.Date2;

/**
 *
 * @author MathSky
 */
public class SalariedEmployee extends Employee{
    private double weeklySalary;

    public SalariedEmployee(double weeklySalary, String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        super(firstName, lastName, socialSecurityNumber, birthday);
        this.weeklySalary = weeklySalary;
    }
    
   public void setWeeklySalary( double salary ){
       if ( salary >= 0.0 ) weeklySalary = salary;
       else throw new IllegalArgumentException("Weekly salary must be >= 0.0" );
   }

    public double getWeeklySalary() {
        return weeklySalary;
    }

    @Override
    public double getPaymentAmount()
    {
    return getWeeklySalary();
    } 

    @Override
    public String toString()
    {
    return String.format( "salaried employee: %s\n%s: $%,.2f",
    super.toString(), "weekly salary", getWeeklySalary() );
    } 
}
