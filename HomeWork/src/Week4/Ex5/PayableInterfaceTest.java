/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week4.Ex5;

import Week4.Ex2.Date2;
import Week4.Ex6.Payable;

/**
 *
 * @author MathSky
 */
public class PayableInterfaceTest
 {
    public static void main( String[] args ){
        Payable[] payableObjects = new Payable[ 4 ];
        payableObjects[ 0 ] = new Invoice( "01234", "seat", 2, 375.00 );
        payableObjects[ 1 ] = new Invoice( "56789", "tire", 4, 79.95 );

        Date2 d1=new Date2(12,2,1990);
        Date2 d2=new Date2(1,2,1985);
        Date2 d3=new Date2(1,1,1967);
        Date2 d4=new Date2(12,2,1977);
        
        payableObjects[ 2 ] =new SalariedEmployee( 800.00,"John", "Smith", "111-11-1111",d1 );
        payableObjects[ 3 ] =new BasePlusCommissionEmployee( 1800.00,12.4,2.3,"Lisa", "Barnes" ,"111-11-8881",d2 );
        



         System.out.println( "Invoices and Employees processed polymorphically:\n" );


        for ( Payable currentPayable : payableObjects )
        {
            System.out.printf( "%s \n%s: $%,.2f\n\n", currentPayable.toString(),"payment due",currentPayable.getPaymentAmount());
            if(currentPayable instanceof BasePlusCommissionEmployee){
                BasePlusCommissionEmployee bEmployee= (BasePlusCommissionEmployee) currentPayable;
                bEmployee.setBaseSalary(bEmployee.getBaseSalary()*1.01);
                System.out.printf("new base salary with 10%% increase is: $%,.2f\\n\",\n", bEmployee.getBaseSalary() );
            };
        }
    }
} // end class 