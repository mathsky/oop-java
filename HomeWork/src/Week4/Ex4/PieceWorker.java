/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week4.Ex4;

import Week4.Ex2.Date2;

public class PieceWorker extends Week4.Ex2.Employee {
    private double wage;
    private double pieces;

    public PieceWorker(double wage, double pieces, String firstName, String lastName, String socialSecurityNumber, Date2 birthday) {
        super(firstName, lastName, socialSecurityNumber, birthday);
        this.wage = wage;
        this.pieces = pieces;
    }

    public double getWage() {
        return wage;
    }

    public double getPieces() {
        return pieces;
    }

    @Override
    public double earnings() {
        return getWage() * getPieces();
    }

    @Override
    public String toString() {
        return String.format( "Piece Worker: %s\n%s: $%,.2f\n%s: %,.2f", super.toString(), 
                "Wage per piece", getWage(), "Pieces produced", getPieces());
    }  
}
