/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 4 . 32 (Checkerboard Pattern of Asterisks) Write an application that uses only the output statements
 * System.out.print("* ");
 * System.out.print( " " );
 * System.out.println();
 * to display the checkerboard pattern that follows. 
 * 
 * @author MathSky
 */
public class Ex3 {
    public static void main(String[] agrs){
        for(int i=1;i<=8;i++){
            if(i%2==0) System.out.print(" ");
            for(int j=1;j<=8;j++){
                System.out.print("* ");              
            }
            System.out.println();
        }
    }
}
