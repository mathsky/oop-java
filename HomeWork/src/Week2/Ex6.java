/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 4 . 38 (Enforcing Privacy with Cryptography) 
 * @author MathSky
 */
import java.util.Scanner;
public class Ex6 {
    public static void encrypt(int n){
        int a[] = new int[5];
        int m=n;
        for(int i=4;i>0;i--){
            a[i]=m%10;m=m/10;
        }
        for(int i=1;i<=4;i++) {
            a[i]=(a[i]+7)%10;
        }
        int t=a[1];a[1]=a[3];a[3]=t;
        t=a[2];a[2]=a[4];a[4]=t;
        
        for(int i=1;i<=4;i++) System.out.print(a[i]);
        System.out.println();
    }
    public static void decrypt(int n){
        int a[] = new int[5];
        int m=n;
        for(int i=4;i>0;i--){
            a[i]=m%10;m=m/10;
        }
        int t=a[1];a[1]=a[3];a[3]=t;
        t=a[2];a[2]=a[4];a[4]=t;
        
        int arr[]={3,4,5,6,7,8,9,0,1,2};
        for(int i=1;i<=4;i++) {
            a[i]=arr[a[i]];
        }
        
        
        for(int i=1;i<=4;i++) System.out.print(a[i]);
        System.out.println();
    }
    public static void main(String[] agrs){
        Scanner inp =new Scanner(System.in);
        System.out.print("Nhap vao so 4 chu so: ");
        int n=inp.nextInt();
        encrypt(n);
        System.out.println();
        System.out.print("Nhap vao so 4 chu so: ");
        n=inp.nextInt();
        decrypt(n);
    }
}
