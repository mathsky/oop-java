/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 4. 37 (Factorial) 
 * a) Write an application that reads a nonnegative integer and computes and prints its factorial.
 * b) Write an application that estimates the value of the mathematical constant e by using
 * Taylor formula. Allow the user to enter the number of terms to calculate.
 * c) Write an application that computes the value of pow(e,x) by using the Taylor formula.
 * Allow the user to enter the number of terms to calculate.
 * @author MathSky
 */
import java.util.Scanner;
public class Ex5 {
    public static void main(String[] agrs){
        //a)
        System.out.println("a)---------- ");
        Scanner inp =new Scanner(System.in);
        System.out.print("n= ");
        int n=inp.nextInt();
        for(int i=n-1;i>0;i--) n*=i;
        System.out.printf("n!= %d\n",n);
        //b)
        System.out.println("b)---------- ");
        System.out.print("Terms to cal e:  ");
        n=inp.nextInt();
        double e=0,t=1;
        for(int i=1;i<=n+1;i++){
            e+=t;t=t/i;
        }
        System.out.printf("e= %f\n",e);
        //c)
        System.out.println("c)---------- ");
        double ex=0,tx=1,x;
        System.out.print("x=  ");
        x=inp.nextDouble();
        System.out.print("Terms to cal e^x:  ");
        n=inp.nextInt();
        for(int i=1;i<=n+1;i++){
            ex+=tx;tx=tx*x/i;
        }
        System.out.printf("e^x= %f\n",ex);
    }
}
