/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 4.34 (What’s Wrong with This Code?) What is wrong with the following statement? Provide the
 * correct statement to add one to the sum of x and y.
 * System.out.println( ++(x + y) );
 * 
 * @author MathSky
 */
public class Ex4 {
    public static void main(String[] agrs){
        int x=2,y=3;
        System.out.println( (++x + y) );
        //Hoac
        x=2;y=3;
        System.out.println( (x + ++y) );
        
    }
}
