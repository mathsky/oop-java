/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * (Printing the Decimal Equivalent of a Binary Number) Write an application that inputs an
 * integer containing only 0s and 1s (i.e., a binary integer) and prints its decimal equivalent. [Hint: Use
 * remainder and division operators to pick off the binary number’s digits one at a time, from right
 * left. In the decimal number system, the rightmost digit has a positional value of 1 and the next
 * it to the left a positional value of 10, then 100, then 1000, and so on. The decimal number 234
 * be interpreted as 4 * 1 + 3 * 10 + 2 * 100. In the binary number system, the rightmost digit has
 * ositional value of 1, the next digit to the left a positional value of 2, then 4, then 8, and so on.
 * decimal equivalent of binary 1101 is 1 * 1 + 0 * 2 + 1 * 4 + 1 * 8, or 1 + 0 + 4 + 8 or, 13.]
 * @author MathSky
 */
import java.util.Scanner;
import static java.lang.Math.pow;
public class Ex2 {
    public static void main(String[] agrs){
        Scanner inp =new Scanner(System.in);
        System.out.print("Binary: ");
        String s= inp.nextLine();
        int a=0;
        for(int i=s.length()-1;i>=0;i--){
            if(s.charAt(i)=='1') a+=pow(2,s.length()-i-1);
        }
        System.out.printf("Decimal: %d\n",a);
    }
}
