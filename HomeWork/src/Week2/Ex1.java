/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 4 . 30 (Palindromes) A palindrome is a sequence of characters that reads the same backward as forward. For example, each of the following five-digit integers is a palindrome: 12321, 55555, 45554 and 11611.
 * Write an application that reads in a five-digit integer and determines whether it’s a palindrome. If the number is not five digits long, display an error message and allow the user to enter a new value.
 *
 * @author MathSky
 */
import java.util.Scanner;
public class Ex1 {
    public static void main(String[] agrs){
        Scanner inp=new Scanner(System.in);
        int a;
        do{
            System.out.print("Nhap so co 5 chu so: ");
            a=inp.nextInt();
            if(a<10000 || a>99999) System.out.println("Nhap sai !");
        } while(a<10000 || a>99999);
        int b5=a%10,b4=(a%100)/10;
        int b1=a/10000,b2=(a/1000)%10;
        if(b1==b5 && b2==b4) System.out.println("Palindromes!");
        else System.out.println("Not Palindromes!");
    }
}
