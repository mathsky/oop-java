/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

import static java.lang.Math.pow;

/**
 * 5.20 (Calculating the Value of π 
 * @author MathSky
 */
public class Ex8 {
    public static void main(String[] agrs){
        double arr[] = new double[200001];
        arr[0]=0;double t=4;
        for(int i=1;i<200001;i++){
            arr[i]=arr[i-1]-pow(-1,i)*4/(2*i-1);
        }
        for(int i=1;i<200000;i++){
            System.out.printf("n= %d -> pi= %f\n",i,arr[i]);
        }
    }
}
