/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 5 . 15 (Triangle Printing Program)
 * @author MathSky
 */
public class Ex7 {
    public static void main(String[] agrs){
        //a)
        for(int i=1;i<=10;i++){
            for(int j=1;j<=i;j++) System.out.print('*');
            System.out.println();
        }
        //b)
        for(int i=1;i<=10;i++){
            for(int j=i;j<=10;j++) System.out.print('*');
            System.out.println();
        }
        //c)
        for(int i=1;i<=10;i++){
            for(int j=1;j<i;j++) System.out.print(' ');
            for(int j=i;j<=10;j++) System.out.print('*');
            System.out.println();
        }
        //d)
        for(int i=1;i<=10;i++){
            for(int j=i;j<10;j++) System.out.print(' ');
            for(int j=1;j<=i;j++) System.out.print('*');
            System.out.println();
        }
    }
}
