/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week2;

/**
 * 7.12 (Duplicate Elimination) Use a one-dimensional array to solve the following problem:
 * Write an application that inputs five numbers, each between 10 and 100, inclusive. As each number
 * read, display it only if it’s not a duplicate of a number already read. Provide for the “worst case,”
 * which all five numbers are different. Use the smallest possible array to solve this problem. Display
 * complete set of unique values input after the user enters each new value.
 * 
 * @author MathSky
 */
import java.util.Scanner;
public class Ex9 {
    public static void main(String[] agrs){
        Scanner inp =new Scanner(System.in);
        int arr[] = new int[5],j=-1;int t;
        for(int i=0;i<5;i++){
            do{
                System.out.printf("Nhap so thu %d: ",i+1);
                t=inp.nextInt();
                if(t>100 || t<10) System.out.println("Nhap lai!@@@");
            } while(t>100 || t<10);
            boolean kt=false;
            for(int k=j;k>=0;k--)
                if(t==arr[k]) {
                    kt=true;break;
                }
            if(!kt) {j++;arr[j]=t;} 
        }
        System.out.print("set of unique values: ");
        for(int i=0;i<=j;i++) System.out.printf("%d ",arr[i]);
        if(j==4) System.out.println("Worse case!");
    }
}
