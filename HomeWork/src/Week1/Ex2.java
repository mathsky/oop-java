/*
 * MS
 */
package week1;

/**
 *(Displaying Shapes with Asterisks
 * @author MathSky
 */
public class Ex2 {
    public static void main (String[] args){
        System.out.println("*********      ***        *         *");
        System.out.println("*       *    *     *     ***       * *");
        System.out.println("*       *   *       *   *****     *   *");
        System.out.println("*       *   *       *     *      *     *");
        System.out.println("*       *   *       *     *     *       *");
        System.out.println("*       *   *       *     *      *     *");
        System.out.println("*       *   *       *     *       *   *");
        System.out.println("*       *    *     *      *        * *");
        System.out.println("*********      ***        *         *");
        
    }
}
