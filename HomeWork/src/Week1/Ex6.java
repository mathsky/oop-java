/*
 * MS
 */
package week1;

/**
 *Diameter, Circumference and Area of a Circle
 * @author MathSky
 */
import static java.lang.Math.PI;
import java.util.Scanner;
public class Ex6 {
    public static void main (String[] agrs){
        Scanner input= new Scanner(System.in);
        int r,d;
        float c,s;
        System.out.print("Nhap vo ban kinh r: ");
        r= input.nextInt();
        System.out.printf("Diameter: %d\n",2*r);
        System.out.printf("Circumference: %f\n",2*PI*r);
        System.out.printf("Area: %f\n",PI*r*r);
        
    }
}
