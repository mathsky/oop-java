/*
 * MS
 */
package week1;

/**
 * Separating the Digits in an Integer
 * @author MathSky
 */
import java.util.Scanner;
public class Ex8 {
    public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        int n,t;
        System.out.print("Nhap vao mot so co 5 chu so: ");
        n= input.nextInt();
        t=n/10000;n-=t*10000;
        System.out.printf("%d   ",t);
        t=n/1000;n-=t*1000;
        System.out.printf("%d   ",t);
        t=n/100;n-=t*100;
        System.out.printf("%d   ",t);
        t=n/10;n-=t*10;
        System.out.printf("%d   ",t);
        System.out.printf("%d\n",n);

    }
}
