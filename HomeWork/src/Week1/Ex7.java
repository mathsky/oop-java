/*
 * MS
 */
package week1;

/**
 * Display the integer equivalents of the following: A B C a b c 0 1 2 $ * + / and the blank character
 * @author MathSky
 */
public class Ex7 {
    public static void main(String[] agrs){
        System.out.println("Ky tu ----> Gia tri nguyen");
        System.out.printf(" %c ------ %d\n", 'A' , ( (int) 'A' ) );
        System.out.printf(" %c ------ %d\n", 'B' , ( (int) 'B' ) );
        System.out.printf(" %c ------ %d\n", 'C' , ( (int) 'C' ) );
        System.out.printf(" %c ------ %d\n", 'a' , ( (int) 'a' ) );
        System.out.printf(" %c ------ %d\n", 'b' , ( (int) 'b' ) );
        System.out.printf(" %c ------ %d\n", 'c' , ( (int) 'c' ) );
        System.out.printf(" %c ------ %d\n", '0' , ( (int) '0' ) );
        System.out.printf(" %c ------ %d\n", '1' , ( (int) '1' ) );
        System.out.printf(" %c ------ %d\n", '2' , ( (int) '2' ) );
        System.out.printf(" %c ------ %d\n", '$' , ( (int) '$' ) );
        System.out.printf(" %c ------ %d\n", '*' , ( (int) '*' ) );
        System.out.printf(" %c ------ %d\n", '+' , ( (int) '+' ) );
        System.out.printf(" %c ------ %d\n", '/' , ( (int) '/' ) );
        System.out.printf(" %s ------ %d\n", "[blank]" , ( (int) ' ' ) );
    }
           
}
