/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;

/**
 * 8 . 4 (Rectangle Class) 
 * @author MathSky
 */
public class Ex3 {
    public static void main(String[] agrs){
    Recangle r1 = new Recangle();
    Recangle r2 = new Recangle();
    r1.setWidth(2.3);r1.setLength(5.4);
    r2.setLength(12);r2.setWidth(14);
    System.out.println(r2.perimeter());
    System.out.println(r2.area());
    }
}

class Recangle{
    private double width;
    private double length;

    public Recangle() {
        width=1;
        length=1;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        if(width>0.0 && width <20.0) this.width = width;
        else System.out.printf("Out of range!");
    }

    public void setLength(double length) {
        
        if(length>0.0 && length <20.0) this.length = length;
        else System.out.printf("Out of range!");
    }
    public double perimeter(){
        return 2*(width+length);
    }
    public double area(){
        return width*length;
    }
}
