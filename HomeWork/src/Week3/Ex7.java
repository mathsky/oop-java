/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;


import static Week3.Rational.*;

/**
 * 8.15 Rational Number
 * @author MathSky
 */
public class Ex7 {
    public static void main(String[] args) {
        Rational r1,r2,r3;
        r1= new Rational(2,4);
        r2= new Rational(1,5);
        System.out.println(r1.toStringFraction());
        System.out.println(r1.toStringFloat(3));
        System.out.println(add(r1,r2).toStringFraction());
        System.out.println(subtract(r1,r2).toStringFraction());
        System.out.println(multiply(r1,r2).toStringFraction());
        System.out.println(divide(r1,r2).toStringFraction());
    }
}

class Rational{
    private int numerator;
    private int denominator;
    
    public Rational(){
        denominator=1;
    }
    public Rational(int n,int d){
        int u=Ucln(n,d);
        this.numerator=n/u;
        this.denominator=d/u;
    }
    private static int Ucln(int a,int b){
        while(a*b!=0){
            if(a>b) a=a%b;else b=b%a;
        }
        return (a+b);
    }
    public static Rational add(Rational r1,Rational r2){
        int n,d,u;
        Rational rt= new Rational();
        n=r1.numerator*r2.denominator+r2.numerator*r1.denominator;
        d=r1.denominator*r2.denominator;
        u=Ucln(n,d);
        rt.numerator=n/u;
        rt.denominator=d/u;
        return rt;
    }
    public static Rational subtract(Rational r1,Rational r2){
        int n,d,u;
        Rational rt= new Rational();
        n=r1.numerator*r2.denominator-r2.numerator*r1.denominator;
        d=r1.denominator*r2.denominator;
        u=Ucln(n,d);
        rt.numerator=n/u;
        rt.denominator=d/u;
        return rt;
    }
    public static Rational multiply(Rational r1,Rational r2){
        int n,d,u;
        Rational rt= new Rational();
        n=r1.numerator*r2.numerator;
        d=r1.denominator*r2.denominator;
        u=Ucln(n,d);
        rt.numerator=n/u;
        rt.denominator=d/u;
        return rt;
    }
    public static Rational divide(Rational r1,Rational r2){
        int n,d,u;
        Rational rt= new Rational();
        n=r1.numerator*r2.denominator;
        d=r1.denominator*r2.numerator;
        u=Ucln(n,d);
        rt.numerator=n/u;
        rt.denominator=d/u;
        return rt;
    }
    public String toStringFraction(){
        return String.format("%d/%d", this.numerator,this.denominator);
    }
    public String toStringFloat(int n){
        return String.format("%.3f",(float)this.numerator/this.denominator);
    }
}
