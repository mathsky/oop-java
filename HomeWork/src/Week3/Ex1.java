/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;

/**
 * 3.14.Employee Class
 * @author MathSky
 */
public class Ex1 {
    public static void main(String[] agrs){
        Employee em1=new Employee("Messi","Leonel",10);
        Employee em2;
        em2 = new Employee("Cristiano","Ronaldo",7.7);
        System.out.println("Thong tin luong: ");
        System.out.printf("Name: %s  Salary: %.2f\n",em1.getName(),em1.getSalary());
        System.out.printf("Name: %s  Salary: %.2f\n",em2.getName(),em2.getSalary());
        System.out.println("--------------------------------- ");
        System.out.println("Thong tin luong sau khi tang 10%: ");
        em1.setSalary(em1.getSalary()*1.1);
        em2.setSalary(em2.getSalary()*1.1);
        System.out.printf("Name: %s  Salary: %.2f\n",em1.getName(),em1.getSalary());
        System.out.printf("Name: %s  Salary: %.2f\n",em2.getName(),em2.getSalary());
    }
}


class Employee{
    private String name;
    private String lastName;
    private double salary;
    public Employee(){
        name="";lastName="";salary=0.0;
    }

    public Employee(String name, String lastName, double salary) {
        this.name = name;
        this.lastName = lastName;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSalary(double salary) {
        if(salary>0) this.salary = salary;
    }
    
}

