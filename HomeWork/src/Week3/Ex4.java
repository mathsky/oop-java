/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;

/**
 * 8 . 11 (Complex Numbers) 
 * @author MathSky
 */
public class Ex4 {
    public static void main(String[] agrs){
        Complex c1= new Complex(1,2);
        Complex c2= new Complex(2,2);
        System.out.print("c1 =");c1.print();
        System.out.print("\nc2 =");c2.print();
        c1.add(c2);
        System.out.print("\nc1+c2= ");c1.print();
        c1.setRealPart(1);c1.setImageinaryPart(2);
        c1.subtract(c2);
        System.out.print("\nc2-c1= ");c1.print();
        
    }
}

class Complex{
    private double realPart;
    private double imageinaryPart;

    public Complex() {
    }

    public Complex(double realPart, double imageinaryPart) {
        this.realPart = realPart;
        this.imageinaryPart = imageinaryPart;
    }

    public double getRealPart() {
        return realPart;
    }

    public double getImageinaryPart() {
        return imageinaryPart;
    }

    public void setRealPart(double realPart) {
        this.realPart = realPart;
    }

    public void setImageinaryPart(double imageinaryPart) {
        this.imageinaryPart = imageinaryPart;
    }
    public void print(){
        System.out.printf("(%.3f,%.3f)",realPart,imageinaryPart);
    }
    public void add(Complex c){
        this.setRealPart(c.getRealPart()+this.realPart);
        this.setImageinaryPart(c.getImageinaryPart()+this.getImageinaryPart());
    }
    public void subtract(Complex c){
        this.setRealPart(c.realPart-this.realPart);
        this.setImageinaryPart(c.imageinaryPart-this.imageinaryPart);
    }
}