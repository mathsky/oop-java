/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;

/**
 * 3 . 15 (Date Class) 
 * @author MathSky
 */
public class Ex2 {
    public static void main(String[] agrs){
        System.out.println("month/day/year");
        Date d1 =new Date(0,2,2014);
        Date d2 =new Date(2,29,2012);
        d1.Display();
        d2.Display();
    }
}

class Date{
    private int month;
    private int day;
    private int year;
    public Date(){
        month=1;day=1;year=2015;
    }

    public Date(int month, int day, int year) {
        if (month>13)this.month = 12;
        else if(month<1) this.month=1;else this.month=month;
        //------------------------------
        if(year<0) this.year=0;else this.year=year;
        boolean nhuan=false;
        if(year %4==0)
            if(year%100==0) 
                if(year%400==0) nhuan=true;else nhuan=false;
            else nhuan =true;
        else nhuan=false;
        //-------------------------------
        switch (this.month){
            case 1:case 3:case 5:case 7: case 8:case 10:case 12:
                if(day>31) this.day=31;
                else if(day<1) this.day=1;else this.day=day;
            case 4: case 6: case 9: case 11:
                if(day>30) this.day=30;
                else if(day<1) this.day=1;else this.day=day;
            case 2:
                if(nhuan) if(day<1) this.day=1;
                        else if(day>29) this.day=29;
                        else this.day=day;
                else if(day<1) this.day=1;
                        else if(day>28) this.day=28;
                        else this.day=day;
        }
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }
    public void Display(){
        System.out.printf("%d/%d/%d\n",this.month,this.day,this.year);
    }
}

