/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;

/**
 * 8 . 13 (Set of Integers)
 * @author MathSky
 */
import static Week3.IntegerSet.intersection;
import static Week3.IntegerSet.union;
import java.util.Random;
import java.lang.Integer;
public class Ex5 {
    public static void main(String[] agrs){
        IntegerSet is1= new IntegerSet();
        IntegerSet is2= new IntegerSet();
        Random r= new Random();
        for(int i=0;i<5;i++) is1.insertElement(r.nextInt(101));
        for(int i=0;i<5;i++) is2.insertElement(r.nextInt(101));
        System.out.println("set 1: "+is1.toString());
        System.out.println("set 2: "+is2.toString());
        System.out.printf("Union: %s\n",union(is1,is2).toString());
        System.out.printf("Intersection: %s\n",intersection(is1,is2).toString());
        System.out.println(is1.isEqualTo(is2)?'1':'0');
    }
}

class IntegerSet{
    private boolean a[];
    
    public IntegerSet(){
        a = new boolean[101];
    }

    public boolean[] getA() {
        return a;
    }
    public void insertElement(int k){
        if(k>=0 && k<=100) this.a[k]=true;
        else 
            throw new IllegalArgumentException("out of range!");
    }
    public void deleteElement(int k){
        if(k>=0 && k<=100) this.a[k]=false;
    }
    public static IntegerSet union(IntegerSet i1,IntegerSet i2){
        IntegerSet it= new IntegerSet();
        for(int i=0;i<101;i++) if(i1.a[i] || i2.a[i]) it.a[i]=true;
        return it;
    }
    public static IntegerSet intersection(IntegerSet i1,IntegerSet i2){
        IntegerSet it= new IntegerSet();
        for(int i=0;i<101;i++) if(i1.a[i] && i2.a[i]) it.a[i]=true;
        return it;
    }
    public String toString(){
        String s="";
        for(int i=0;i<101;i++) if (this.a[i]) s=s+Integer.toString(i)+' ';
        if(s=="") s="---";
        return s;
    }
    public boolean isEqualTo(IntegerSet is){
        for(int i=0;i<101;i++) if(this.a[i]!=is.a[i]) return false;
        return true;
    }
}
