/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Week3;
import static Week3.HugeInteger.*;

/**
 * 8 . 16 (Huge Integer Class) 
 * @author MathSky
 */
public class Ex8 {
    public static void main(String[] args) {
    HugeInteger h1= new HugeInteger();
    HugeInteger h2= new HugeInteger();
    HugeInteger h3= new HugeInteger();
    h1.parse("1100");
    h2.parse("1");
    System.out.println("h1: "+h1.toString());
    System.out.println("h2: "+h2.toString());
    System.out.println("h1+h2="+add(h1,h2).toString());
    if(h1.isEqualto(h2)) System.out.println("h1=h2");
    else if(h1.isGreaterThan(h2)) System.out.println("h1>h2");
    else System.out.println("h1<h2");
    System.out.println("h1-h2="+subtract(h1,h2).toString());
    }
}

class HugeInteger{
    private char[] D;
    private int len;
    
    public HugeInteger(){
        D =new char[40];
        len=0;
    }
    public void parse(String s){
        
        for(int i=s.length()-1;i>=0;i--){
            D[len++]=s.charAt(i);
        }
    }
    public String toString(){
        String s="";
        for(int i=0;i<len;i++) s=D[i]+s;
        return s;
    }

    public char[] getD() {
        return D;
    }
    public int getLen() {
        return len;
    }
    
    public static HugeInteger add(HugeInteger h1,HugeInteger h2){
        HugeInteger kq= new HugeInteger();
        int l1=h1.len,l2=h2.len,l=l1<l2?l1:l2;
        int nho=0;
        for(int i=0;i<l;i++){
            int t=(h1.D[i]+h2.D[i]+nho-96);
            kq.D[kq.len++]=(char) (t%10+48);
            nho=t/10;
        }
        if(l1>l2) for(int i=l2;i<l1;i++){
            int t=(h1.D[i]+nho-48);
            kq.D[kq.len++]=(char) (t%10+48);
            nho=t/10;
        }
        if(l1<l2) for(int i=l1;i<l2;i++){
            int t=h2.D[i]+nho-48;
            kq.D[kq.len++]=(char) (t%10+48);
            nho=t/10;
        }
        if(nho!=0) kq.D[kq.len++]=(char)(nho+48);
        return kq;
    }
    public static HugeInteger subtract(HugeInteger h1,HugeInteger h2){
        //h1>=h2
        HugeInteger kq= new HugeInteger();
        if(h1.isEqualto(h2)) return kq;
        int nho=0;
        for(int i=0;i<h2.len;i++){
                int t=(h1.D[i]-h2.D[i]+10-nho)%10;
                if(h1.D[i]<h2.D[i]+nho) nho=1;else nho=0;
                kq.D[kq.len++]=(char)(t+48);
            }
        
        for(int i=h2.len;i<h1.len;i++){
            int t=(h1.D[i]-48+10-nho)%10;
            if((h1.D[i]-48)<nho) nho=1;else nho=0;
            
            kq.D[kq.len++]=(char)(t+48);
        }
        return kq;
    }
    public boolean isEqualto(HugeInteger h){
        return (this.toString().equals(h.toString()));
    }
    public boolean isGreaterThan(HugeInteger h){
        if(this.len>h.len) return true;
        if(this.len<h.len) return false;
        for(int i=this.len-1;i>=0;i--){
            if(this.D[i]!=h.D[i]) return (this.D[i]>h.D[i]);
        }
        throw new IllegalArgumentException("Hai so bang nhau!");
    }
    public boolean isLessThan(HugeInteger h){
        if(this.len<h.getLen()) return true;
        if(this.len>h.getLen()) return false;
        for(int i=this.len-1;i>=0;i--){
            if(this.D[i]!=h.D[i]) return (this.D[i]<h.D[i]);
        }
        throw new IllegalArgumentException("Hai so bang nhau!");
    }
}
