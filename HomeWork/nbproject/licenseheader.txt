<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}		****      ****       *******
${licensePrefix}		** **    ** **       **
${licensePrefix}		**    *  *    **        ***
${licensePrefix}		**      *      **               **
${licensePrefix}		* *             **     *******
<#if licenseLast??>
${licenseLast}
</#if>
