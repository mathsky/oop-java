/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B1;

import java.util.Scanner;

/**
 *
 * @author MathSky
 */
public class Vehicle {
    private String mark;
    private String model;
    private double price;

    public Vehicle() {
    }
    
    public Vehicle(String mark, String model, double price) {
        this.mark = mark;
        this.model = model;
        this.price = price;
    }

    public String getMark() {
        return mark;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public void input(){
        Scanner inp=new Scanner(System.in);
        System.out.printf("Enter vehicle'information\nMark:  ");
        mark=inp.nextLine();
        System.out.print("Model: ");model=inp.nextLine();
        System.out.print("Price: ");price=inp.nextDouble();
    }
    
    public void display(){
        System.out.printf("Vehicle'information:\nMark:  %s ",mark);
        System.out.printf(" Model: %s ",model);
        System.out.printf(" Price: %.2f ",price);
    }
}
