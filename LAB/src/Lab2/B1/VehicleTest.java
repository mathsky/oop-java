/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B1;

import java.util.Scanner;

/**
 *
 * @author MathSky
 */
public class VehicleTest {
    public static void main(String[] args) {
        VehicleCollection vc= new VehicleCollection();
        Scanner inp=new Scanner(System.in);
        System.out.print("How many Vehicle do you want? ");
        int n=inp.nextInt();
        for(int i=0;i<n;i++){
            vc.input();
        }
        
        vc.display();
    }
}
