/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B1;

import java.util.Scanner;

/**
 *
 * @author MathSky
 */
public class Car extends Vehicle{
    private String color;

    public Car() {
    }

    public Car(String color, String mark, String model, double price) {
        super(mark, model, price);
        this.color = color;
    }
    
    public void input(){
        Scanner inp=new Scanner(System.in);
        super.input();
        System.out.print("Color: ");color= inp.nextLine();
    }
    
    public void display(){
        super.display();
        System.out.println("Color: "+color);
    }
        
}
