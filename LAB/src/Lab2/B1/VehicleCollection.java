/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B1;

import java.util.Scanner;

/**
 *
 * @author MathSky
 */
public class VehicleCollection {
    private Vehicle[] list;
    private int count=0;
    
    final int MAX=100;

    public VehicleCollection() {
        list = new Vehicle[MAX];
    }
    
    public void input(){
        Scanner inp= new Scanner(System.in);
        System.out.println("Car(1) or Truck(else) ?: ");
        Byte choose=inp.nextByte();
        Car c= new Car();
        Truck t= new Truck();
        if(choose==1) {c.input();add(c);}
        else {t.input();add(t);}       
    }
    
    public void add(Vehicle vh){
        if(count<MAX) list[count++]=vh;
        else throw new IllegalArgumentException("List full!");
    }
    
    public void display(){
        for(int i=0;i<count;i++){
            System.out.println("*******************************");
            list[i].display();
        }
    }
}
