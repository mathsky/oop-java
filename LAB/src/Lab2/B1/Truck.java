/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B1;

import java.util.Scanner;

/**
 *
 * @author MathSky
 */
public class Truck extends Vehicle{
    private int truckload;

    public Truck() {
    }
    
    public Truck(int truckload, String mark, String model, double price) {
        super(mark, model, price);
        this.truckload = truckload;
    }  
   
    public void input(){
        super.input();
        Scanner inp=new Scanner(System.in);
        System.out.print("truckload: ");
        truckload=inp.nextInt();
    }
    
    public void display(){
        super.display();
        System.out.printf("trucload: %d\n",truckload);
    }
}
