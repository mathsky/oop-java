/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B4;

/**
 *
 * @author MathSky
 */
public class MyLinkedQueue {
    private Node head;
    private Node tail;
    private int count;

    public MyLinkedQueue() {
        head=tail=null;
        count=0;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public int getCount() {
        return count;
    }
    
    public int size(){
        return count;
    }
    
    public boolean isEmpty(){
        return head==null;
    }
    
    public void enqueue(int e){
        Node t= new Node();
        t.setValue(e);
        if(isEmpty()){
            head=tail=t;
        }
        else{
            t.setNext(tail);
            tail.setPrevious(t);
            tail=t;
        }
        count++;
    }
    
    public int dequeue(){
        int k=head.getValue();
        if(count==1) head=tail=null;
        else head.getPrevious().setNext(null);
        count--;
        return k;
    }
    
    public int first(){
        return head.getValue();
    }
    
    public void display(){
        Node t=head;
        while(t!=null){
            System.out.printf("%d ",t.getValue());
            t=t.getPrevious();
        }
    }
}

class Node{
    private int value;
    private Node next;
    private Node previous;

    public Node() {
    }

    public Node(int value) {
        this.value = value;
        next=previous=null;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }
    
    public void setNext(Node next) {
        this.next = next;
    }
    
    
}