/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B4;

/**
 *
 * @author MathSky
 */
public interface MyQueue {
    int size();
    boolean isEmpty();
    void enqueue(int e);
    int dequeue();
    int first();
}
