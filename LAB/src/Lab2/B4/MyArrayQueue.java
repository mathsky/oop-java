/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B4;

/**
 *
 * @author MathSky
 */
public class MyArrayQueue implements MyQueue{
    private int [] list;
    private int head;
    private int tail;
    
    final int MAX=100;

    public MyArrayQueue() {
        list =new int[MAX];
        head=tail=-1;
    }

    public int[] getList() {
        return list;
    }

    public int getHead() {
        return head;
    }

    public int getTail() {
        return tail;
    }

    public void setList(int[] list) {
        this.list = list;
    }

    public boolean isEmpty(){
        return head==-1;
    }
    
    public int size(){
        int s=tail-head+1;
        return s>=0? s:s+MAX;
    }
    
    public void enqueue(int e){
        tail=(tail+1)%MAX;
        list[tail]=e;
    }
    
    public int dequeue(){
        int k=list[head];
        head=(head+1)%MAX;
        return k;
    }
    
    public int first(){
        return list[head];
    }
    
    
}
