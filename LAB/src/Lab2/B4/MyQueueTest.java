/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B4;

/**
 *
 * @author MathSky
 */
public class MyQueueTest {
    public static void main(String[] args) {
        MyArrayQueue q= new MyArrayQueue();
        q.enqueue(1);
        q.enqueue(2);
        
        MyLinkedQueue lq= new MyLinkedQueue();
        lq.enqueue(1);
        lq.enqueue(2);
        lq.enqueue(3);
        lq.display();
    }
}
