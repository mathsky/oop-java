/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B3;

/**
 *
 * @author MathSky
 */
public class MyLinkedStack implements MyStack{
    private Node head;
    private int count;


    public MyLinkedStack() {
    }

    public Node getHead() {
        return head;
    }

    public int getCount() {
        return count;
    }
    
    
    public int size(){
        return count;
    }
    public boolean isEmpty(){
        return count==0;
    }
    public void push(int e){
        Node t=new Node(e);
        t.setNext(head);
        head=t;
        count++;
    }
    public void pop(){
        if(isEmpty()) throw new IllegalArgumentException("Stack is Empty!");
        head=head.getNext();count--;
    }
    public int top(){
        return head.getValue();
    }
    public void display(){
        Node t=head;
        while(t!=null){
            System.out.printf("%d  ",t.getValue());
            t=t.getNext();
        }
    }
}

class Node{
    private int value;
    private Node next;

    public Node() {
    }

    public Node(int value) {
        this.value = value;
        next=null;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }
    
    
}