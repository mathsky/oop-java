/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B3;

/**
 *
 * @author MathSky
 */
public class MyArrayStack implements MyStack{
    private int[] stack;
    private int count;
    
    final int MAX=100;

    public MyArrayStack() {
        stack =new int[MAX];
    }
    
    public int size(){
        return count;
    }
    public boolean isEmpty(){
        return count==0;
    }
    public void push(int e){
        if(count<MAX){
            stack[count++]=e;
        }
        else throw new IllegalArgumentException("Stack is Full!");
    }
    public int top(){
        return stack[count-1];
    }
    public void pop(){
        count--;
    }
    public String toString(){
        String s="";
        for(int i=0;i<count;i++) s=s+stack[i]+' ';
        return s;
    }
}
