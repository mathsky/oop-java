/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B3;

/**
 *
 * @author MathSky
 */
public interface MyStack {
    int size();
    boolean isEmpty();
    void push(int e);
    int top();
    void pop();
}
