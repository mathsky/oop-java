/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B3;

/**
 *
 * @author MathSky
 */
public class StackTest {
    public static void main(String[] args) {
        MyLinkedStack ls = new MyLinkedStack();
        ls.push(1);
        ls.push(2);
        ls.display();
    }
}
