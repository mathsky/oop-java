/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B2;

/**
 *
 * @author MathSky
 */
public interface MyList {
    int size();
    boolean isEmpty();
    int get(int i);
    void set(int i,int e);
    void add(int i,int e);
    void remove(int i);
}
