/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B2;

/**
 *
 * @author MathSky
 */
public class MyListTest {
    public static void main(String[] args) {
        MyArrayList al=new  MyArrayList(2);
        al.add(0, 10);
        al.add(0, 11);
        System.out.println(al.toString());
        MyDynamicArrayList dl= new MyDynamicArrayList(1);
        dl.add(0, 1);
        dl.add(0, 2);
        dl.add(0, 3);
        System.out.println(dl.toString());

    }
}
