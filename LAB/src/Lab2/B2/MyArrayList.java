/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B2;

/**
 *
 * @author MathSky
 */
public class MyArrayList implements MyList{
    private int[] list ;
    private int count;
    private int length;
    
    public MyArrayList() {
    }
    public MyArrayList(int l){
        length=l;
        list =new int[length];
    }

    public int[] getList() {
        return list;
    }

    public int getCount() {
        return count;
    }

    public int getLength() {
        return length;
    }

    public void setList(int[] list) {
        this.list = list;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setLength(int length) {
        this.length = length;
    }
    
    public int size(){
        return count;
    }
    
    public boolean isEmpty(){
        return count==0;
    }
    
    public int get(int i){
        return list[i];
    }
    
    public void set(int i,int e){
        list[i]=e;
    }
    
    public void add(int i,int e){
        if(count<length){
            for(int j=count;j>i;j--){
                list[j]=list[j-1];
            }
            list[i]=e;
            count++;
        }
        else throw new IllegalArgumentException("List full");
    }
    
    public void remove(int i){
        for(int j=i;j<count;j++){
            list[j]=list[j+1];
        }
        count--;
    }
    
    public String toString(){
        String s="";
        for(int i=0;i<count;i++) s=s+list[i]+" ";
        return s;
    }
}
