/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab2.B2;

/**
 *
 * @author MathSky
 */
public class MyDynamicArrayList extends MyArrayList{

    public MyDynamicArrayList() {
    }
    
    public MyDynamicArrayList(int c){
        setLength(c);
        int [] lt=new int[c];
        setList(lt);
    }
    
    public void resize(int c){
        if(c<=getLength()) throw new IllegalArgumentException("Capacity is smaller current list'length!");
        
        int [] lt= new int[c];
        for(int i=0;i<getCount();i++){
            lt[i]=getList()[i];
        }
        setLength(c);
        setList(lt);
        
    }
    
    public void add(int i,int e){
        if(getCount()<getLength()){
            
            for(int j=getCount();j>i;j--){
                super.set(j, super.getList()[j-1]);
            }
            super.set(i, e);
            setCount(getCount()+1);
        }
        else {
            resize(getLength()+1);
            for(int j=getCount();j>i;j--){
                super.set(j, super.getList()[j-1]);
            }
            super.set(i, e);
            setCount(getCount()+1);
        }
    }
}
