/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 3 (lab3.java): Sử dụng giải thuật Oclid tìm Ước chung lớn nhất của hai số nguyên a và b do người dùng nhập vào. 
 * @author MathSky
 */
import java.util.Scanner;
public class lab1_3 {
    public static void main(String[] agrs){
        Scanner inp=new Scanner(System.in);
        int a,b;
        System.out.println("Nhap vao hai so: ");
        System.out.print("a= ");a=inp.nextInt();
        System.out.print("b= ");b=inp.nextInt();
        while(a*b!=0){
            if(a>b) a=a%b;
            else b=b%a;
        }
        System.out.printf("UCLN(a,b)= %d\n",a+b);
    }
}
