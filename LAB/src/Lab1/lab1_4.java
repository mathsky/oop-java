/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 * Bài 4 (lab4.java): Nhập vào 1 số nguyên, kiểm tra xem số đó có phải là số nguyên tố hay không.
 * @author MathSky
 */
import static java.lang.Math.sqrt;
import java.util.Scanner;

public class lab1_4 {
    public static void main(String[] agrs){
        Scanner inp =new Scanner(System.in);
        System.out.print("Nhap vao mot so: ");
        int a= inp.nextInt();
        boolean T=true;
        for(int i=2;i<=sqrt(a);i++){
            if(a%i==0) {T=false;break;}
        }
        if(T) System.out.printf("%d la so nguyen to!\n",a);
        else System.out.printf("%d khong la so nguyen to!\n",a);
    }
}
