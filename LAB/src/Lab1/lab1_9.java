/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 * Bài 9 (lab9.java): Viết chương trình tính biểu thức đơn giản như 17 + 3 và 3,14159 * 4.7.
 * Các biểu thức sẽ được nhập vào bởi người sử dụng. Đầu vào luôn luôn bao gồm một toán hạng,
 * tiếp theo là một toán tử, tiếp theo là một số toán hạng. Các toán hạng được phép là +, -, *, và /.
 * Chương trình tính giá trị biểu thức người dùng nhập vào cho đến khi người dùng nhập vào 0 là toán hạng đầu tiên.
 * @author MathSky
 */
import java.util.Scanner;
import java.lang.Double;
public class lab1_9 {
    public static int findid(String s){
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)<48) return i;
        }
        return -1;
    }
    public static void main(String[] agrs){
        Scanner inp= new Scanner(System.in);
        double a,b,kq;String s;
        System.out.println("Format: [toan hang][toan tu][toan hang]");
        do{
            System.out.print("Nhap bieu thuc: ");
            s=inp.nextLine();
            int i=findid(s);
            a= Double.parseDouble(s.substring(0,i));
            b= Double.parseDouble(s.substring(i+1,s.length()));
            char c=s.charAt(i);
            switch (c){
                case '+':
                    kq=a+b;break;
                case '-':
                    kq=a-b;break;
                case '*':
                    kq=a*b;break;
                case '/':
                    kq=a/b;break;
                default:
                    System.out.println("Loi toan tu!");
                    kq=0;
                    break;
                    
            }
            System.out.printf("%.3f\n", kq);
        } while(a!=0);
    }
}
