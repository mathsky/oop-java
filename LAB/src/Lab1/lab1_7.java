/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 7 (lab7.java): Giả sử rằng một tập tin có tên "testdata.txt" chứa các thông tin sau:
 * Dòng đầu tiên của tập tin là tên của một học sinh. 
 * Mỗi một trong ba dòng tiếp theo chứa một số nguyên.
 * Các số nguyên là những điểm số của học sinh trên ba kỳ 
 * Viết chương trình đọc thông tin trong các tập tin và hiển thị (trên đầu ra tiêu chuẩn)
 * một tin nhắn có chứa tên của sinh viên và điểm trung bình của học sinh trên ba kỳ thi.
 * 
 * @author MathSky
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class lab1_7 {
    public static void main(String[] agrs){
        File f =new File("testdata.txt");
        try{
            Scanner inp =new Scanner(f);
            String S=inp.nextLine();
            int a=inp.nextInt();
            int b=inp.nextInt();
            int c=inp.nextInt();
            System.out.printf("Ho Ten: %s\nDiem TB: %.2f\n", S,(float)(a+b+c)/3);
        }
        catch(Exception e){
            System.err.println("File not found!");
            System.exit(1);
        }
    }
}
