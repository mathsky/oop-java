/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **              **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Giả sử rằng một tập tin  “sales.dat” chứa thông tin về doanh số bán hàng cho một công ty ở các thành phố khác nhau.
 * Mỗi dòng của tập tin có chứa một tên thành phố, theo sau là một dấu hai chấm (:)
 * tiếp theo là dữ liệu cho thành phố đó. Dữ liệu là một số kiểu double.
 * Tuy nhiên, đối với một số thành phố, không có dữ liệu đã có sẵn. Trong những dòng này,
 * dữ liệu được thay thế bằng một bình luận giải thích lý do tại sao các dữ liệu bị mất.
 * 
 * Viết chương trình tính toán và in tổng doanh thu từ tất cả các thành phố với nhau.
 * Chương trình này cũng cho biết số lượng các thành phố có số liệu không có sẵn. 
 * @author MathSky
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.lang.Double;
public class lab1_10 {
    public static int findid(String s){
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)>=48 && s.charAt(i) <=57) return i;
        }
        return -1;
    }
    public static void main(String[] agrs){
        File f= new File("sales.dat");
        try{
            String s;Double g=0.0;int d=0;
            Scanner inp =new Scanner(f);
            while(inp.hasNext()){
                s=inp.nextLine();
                int i=findid(s);
                if(i==-1) d++;
                else {
                    g+=Double.parseDouble(s.substring(i,s.length()));
                }
            }
            System.out.printf("Income: %.3f\nCity has no data: %d\n", g,d);
        }
        catch(Exception e){
            System.err.println("File Not Found!");
            System.exit(1);
        }
        
    }
}
