/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 5 (lab5.java): Nhập vào một chuỗi
 * 1.	Viết hoa chuỗi
 * 2.	Viết thường chuỗi
 * 3.	Đảo chuỗi
 * 4.	Kiểm tra chuỗi đối xứng
 * 5.	Chuẩn hóa chuỗi
 * 
 * @author MathSky
 */
import java.util.Scanner;
import static java.lang.System.out;
public class lab1_5 {
    public static char charUpper(char c){
        if(c<=122 && c>=97) return ((char)((int)c-32));
        else return c;
    }
    public static char charLower(char c){
        if(c<=90 && c>=65) return ((char)((int)c+32));
        else return c;
    }
    public static String normality(String s){
        String sn="",st;
        st=s.trim();
        int i=0;
        while(i<st.length()){
            sn+=charUpper(st.charAt(i));i++;
            while(i<st.length() && st.charAt(i)!=' '){
                sn+=charLower(st.charAt(i));i++;
            }
            while(i<st.length() && st.charAt(i)==' ') i++;
            if(i<st.length()) sn+=' ';
        }
        return sn;
    }
    public static void main(String[] agrs){
        Scanner inp=new Scanner(System.in);
        out.print("Nhap vao chuoi: ");
        String s=inp.nextLine();
        out.printf("UpperCase: \"%s\"\n", s.toUpperCase());
        out.printf("LowerCase: \"%s\"\n", s.toLowerCase());
        String sr="";
        for(int i=s.length()-1;i>=0;i--) sr+=s.charAt(i);
        out.printf("Reverse: \"%s\"\n", sr);
        if(s.equals(sr)) out.println("Palindrom!");
        else out.println("not Palindrom!");
        out.printf("Normality: \"%s\"\n",normality(s));

    }
}
