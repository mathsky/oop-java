/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 2 (lab2.java): Cho dòng lệnh để người dùng nhập dữ liệu đầu vào từ bàn phím là:
Scanner input = new Scanner( System.in ); 
* Cho câu lệnh để đọc một số nguyên từ dòng nhập là: input.nextInt().
* Hãy viết chương trình nhập 2 số từ bàn phím, in ra kết quả của phép +, -, *, / hai số trên.
* In ra thông báo xem hai số này bằng nhau, lớn hơn hay nhỏ hơn. 

 * @author MathSky
 */
import java.util.Scanner;
public class lab1_2 {
    public static void main(String[] agrs){
        Scanner inp =new Scanner(System.in);
        int a,b;
        System.out.printf("Nhap vao hai so: \n");
        System.out.print("a= ");a=inp.nextInt();
        System.out.print("b= ");b=inp.nextInt();
        System.out.printf("%d+%d= %d\n",a,b,a+b);
        System.out.printf("%d-%d= %d\n",a,b,a-b);
        System.out.printf("%d*%d= %d\n",a,b,a*b);
        System.out.printf("%d/%d= %d\n",a,b,a/b);
        if(a>b) System.out.println("a>b");
        else if(a<b) System.out.println("a<b");
        else System.out.println("a=b");
        
    }
}
