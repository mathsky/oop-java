/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 * Bài 8 (lab8.java): Viết chương trình tìm số nguyên trong  đoạn [1, 10000] 
 * có số lượng ước số lớn nhất và in ra kết quả số đó cùng số lượng các ước. 
 * Trường hợp có nhiều số nguyên thỏa mãn chỉ cần in ra một số.
 * @author MathSky
 */
public class lab1_8 {
    public static void main(String[] agrs){
        int A[] = new int[10001];
        for(int i=1;i<=100;i++){
            for(int j=i;j<10000/i;j++){
                if(i==j) A[i*j]++;
                else A[i*j]+=2;
            }
        }
        int imax=1;
        for(int i=2;i<10001;i++) if(A[i]>A[imax]) imax=i;
        System.out.printf("So: %d co %d Uoc\n",imax,A[imax]);
    }
}
