/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 1 (lab1.java): Hãy viết chương trình nhập tên bạn
 * (ví dụ: Lương Chi Mai) từ bàn phím và hiển thị dòng chữ: “Xin chao ban LƯƠNG CHI MAI” ra màn hình.
 * 
 * @author MathSky
 */
import java.util.Scanner;
public class lab1_1 {
    public static void main(String[] agrs){
        Scanner inp = new Scanner(System.in);
        String s= inp.nextLine();
        System.out.printf("Xin chao ban %s\n",s);
    }
}
