/*
 * 		****      ****       *******
 * 		** **    ** **       **
 * 		**    *  *    **        ***
 * 		**      *      **               **
 * 		* *             **     *******
 */
package Lab1;

/**
 *Bài 6 (lab6.java): Viết chương trình mô phỏng lăn một cặp xúc xắc bằng cách chọn một trong các số nguyên 1, 2, 3, 4, 5, 6 hoặc ngẫu nhiên. 
 * Số chọn ngẫu nhiên đại diện cho số trên con xúc xắc sau khi nó được lăn. Ghi ra màn hình giá trị lăn mỗi con xúc xắc và tổng của chúng
 * @author MathSky
 */
import java.util.Random;
public class lab1_6 {
    public static void main(String[] agrs){
    Random rd= new Random();
    int a=rd.nextInt(6)+1;
    int b=rd.nextInt(6)+1;
    System.out.printf("Xuc xac 1: %d\n",a);
    System.out.printf("Xuc xac 2: %d\n",b);
    System.out.printf("Tong: %d\n",a+b);
    
    }
}
